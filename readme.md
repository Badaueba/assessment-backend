# Desafio

- Nodejs, Express and MongoDB API
- CLI to parse and save products from csv to MongoDB
- React SPA to consume the API

### Change to project branch
> git checkout desafio

**How To Run API + React Client: **

> cd client 

> npm install  

> cd ..

> npm install

> npm start

**How To Run csv CLI: **

> npm run parse-csv

### Used Libraries

- Back End: 
	- webpack to compile ts, 
	- nodemon to watch and rerun
	- concurrently to run many npm scripts at the same time
	- express for api routes
	- mongoose for interact with mongoDB
	- csv-parser to read csv files
	
- Fron End: 
	- React scripts to generate scaffolding app
	- React and react-router-dom to run as SPA 
	- semantic-ui for some components and visual features

